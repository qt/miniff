variables:
  OMP_NUM_THREADS: 2
  PIP_W_CACHE: pip3 --cache-dir=.pip

image: ubuntu:latest

stages:
- configure
- test
- docs
- package
- deploy

configure:
  stage: configure
  tags: ["tn3"]
  before_script:
    - apt-get update -qy
    - apt-get install -y wget
  script:
    - TORCH_VERSION_LATEST=$(wget -O- -q https://download.pytorch.org/whl/torch_stable.html | sed -n "s/^.*torch-\(.*\)%2B.*$/\1/p" | tail -1)
    - echo TORCH=torch==$TORCH_VERSION_LATEST+cpu >> build.env
  artifacts:
    reports:
      dotenv: build.env

test:
  stage: test
  tags: ["tn3"]
  cache:
    key: pip
    paths:
      - .pip
  before_script:
    - apt-get update -qy
    - apt-get install -y build-essential python3-dev python3-pip git
    - $PIP_W_CACHE install $NUMPY $SCIPY $NUMERICALUNITS $MATPLOTLIB cython pytest pytest-cov
    - $PIP_W_CACHE install $TORCH -f https://download.pytorch.org/whl/torch_stable.html
  script:
    - python3 setup.py potentials
    - python3 setup.py build_ext --inplace
    - pip3 install -e .
    - pytest --cov=miniff/
    - coverage xml
  parallel:
    matrix:
      - TORCH: torch==1.5.1+cpu
        NUMPY: numpy==1.18.4
        SCIPY: scipy==1.4.1
        NUMERICALUNITS: numericalunits==1.25
        MATPLOTLIB: matplotlib==3.2.1
      - NUMPY: numpy
        SCIPY: scipy
        NUMERICALUNITS: numericalunits
        MATPLOTLIB: matplotlib
  dependencies:
    - configure
  artifacts:
    reports:
      cobertura: coverage.xml

docs:
  stage: docs
  tags: ["tn3"]
  cache:
    key: pip
    paths:
      - .pip
  before_script:
    - apt-get update -qy
    - apt-get install -y build-essential python3-dev python3-pip git
    - $PIP_W_CACHE install $TORCH -f https://download.pytorch.org/whl/torch_stable.html
    - $PIP_W_CACHE install -r docs/requirements.txt
  script:
    - python3 setup.py build_ext --inplace
    - pip3 install -e .
    - cd docs
    - make html
  artifacts:
    paths:
      - docs/_build/html
    expire_in: 1 day
  dependencies:
    - configure

package:
  stage: package
  tags: ["tn3"]
  cache:
    key: pip
    paths:
      - .pip
  before_script:
    - apt-get update -qy
    - apt-get install -y build-essential python3-dev python3-pip git
    - $PIP_W_CACHE install $TORCH -f https://download.pytorch.org/whl/torch_stable.html
    - $PIP_W_CACHE install -r requirements.txt
  script:
    - python3 setup.py sdist bdist_wheel
  artifacts:
    paths:
      - dist
    expire_in: 1 day

deploy:
  stage: deploy
  tags: ["tn3"]
  cache:
    key: pip
    paths:
      - .pip
  before_script:
    - apt-get update -qy
    - apt-get install -y build-essential python3-dev python3-pip git
  script:
    - cd dist
    - $PIP_W_CACHE install *.whl pytest
    - python3 -m pytest --pyargs miniff
