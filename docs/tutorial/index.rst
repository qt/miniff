Tutorials
=========

.. toctree::
    fitting_1d_function
    computing_descriptors
    classical_potentials
