Welcome to miniff documentation!
================================

``miniff`` uses neural networks to compute energies and forces of atomic systems.
Read more about the method from `Behler and Parrinello <https://doi.org/10.1103/PhysRevLett.98.146401>`_.
Preferred installation method is ``pip``

.. code-block:: bash

   pip install miniff

The source code is hosted at `Quantum Tinkerer <https://gitlab.kwant-project.org/qt/miniff>`_.
Report bugs `here <https://gitlab.kwant-project.org/qt/miniff/-/issues>`_.
BSD license.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   using
   tutorial/index
   design

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
