import numpy as np
import torch

import logging
import sys
import hashlib


def num_grad(scalar_f, x, *args, x_name=None, eps=1e-4, **kwargs):
    """
    Numerical gradient.

    Parameters
    ----------
    scalar_f : function
        A function computing a scalar.
    x : np.ndarray
        The point to compute gradient at.
    x_name : str
        The name of the argument ``x`` in the target function.
    eps : float
        Numerical gradient step.
    *args
    **kwargs
        Other arguments to the function.

    Returns
    -------
    gradient : np.ndarray
        The gradient value(s).
    """
    if x_name is not None and len(args) != 0:
        raise ValueError("x_name is supported for keyword-only input arguments")

    def _target(_x):
        if x_name is None:
            return scalar_f(x, *args, **kwargs)
        else:
            return scalar_f(**{x_name: x}, **kwargs)

    x = np.array(x, dtype=float)
    y = np.array(_target(x))
    gradient = np.empty(y.shape + x.shape, dtype=y.dtype)

    for i in np.ndindex(*x.shape):
        x[i] += eps
        e2 = _target(x)
        x[i] -= 2 * eps
        e1 = _target(x)
        gradient[(Ellipsis,) + i] = (e2 - e1) / 2 / eps
        x[i] += eps
    return gradient


def diag1(a):
    """
    Creates a diagonal tensor from a multidimensional input tensor.

    Parameters
    ----------
    a : np.ndarray
        Diagonal values.

    Returns
    -------
    result : np.ndarray
        The resulting tensor with one more dimension.
    """
    n = len(a)
    result = np.zeros((n, n, *a.shape[1:]), dtype=a.dtype)
    x = np.arange(n)
    result[x, x, ...] = a
    return result


def split2(a, like=None):
    """
    Splits array into two halves.
    
    Parameters
    ----------
    a : np.ndarray
        The original array.
    like : tuple
        A two-tuple with arrays defining shapes
        of the output.

    Returns
    -------
    first : np.ndarray
    second : np.ndarray
        First and second halves.
    """
    if like is None:
        s = len(a) // 2
        return a[:s], a[s:]
    else:
        a1, a2 = like
        assert a.size == a1.size + a2.size
        first = a[:a1.size]
        first.shape = a1.shape
        second = a[a1.size:]
        second.shape = a2.shape
        return first, second


def randsphere(n):
    """
    Random points on a sphere.

    Parameters
    ----------
    n : int
        Sample count.

    Returns
    -------
    result : np.ndarray
        The resulting cartesian coordinates.
    """
    z = np.random.rand(n) * 2 - 1
    phi = np.random.rand(n)
    r = (1 - z ** 2) ** .5
    x = r * np.cos(phi)
    y = r * np.sin(phi)
    return np.concatenate([x[:, None], y[:, None], z[:, None]], axis=1)


def unit_vectors(x, metric=None):
    """
    Unit vectors.

    Parameters
    ----------
    x : np.ndarray
        A collection of vectors.
    metric : np.ndarray
        An optional metric.

    Returns
    -------
    result : np.ndarray
        An array of the same shape as ``x`` with unit
        vectors.
    """
    result = x.copy()
    if metric is None:
        x_norm = np.linalg.norm(result, axis=-1)
    else:
        x_norm = np.einsum("ij,jk,ik->i", result, metric, result) ** .5
    result[x_norm != 0, :] /= x_norm[x_norm != 0, None]
    nz = (x_norm == 0).sum()
    if nz:
        result[x_norm == 0, :] = randsphere(nz)
    return result


def masked_unique(a, return_inverse=False, fill_value=None):
    """
    A proper implementation of `np.unique` for masked arrays.

    Parameters
    ----------
    a : np.ma.masked_array
        The array to process.
    return_inverse : bool
        If True, returns the masked inverse.
    fill_value : int
        An optional value to fill the `return_inverse` array.

    Returns
    -------
    key : np.ndarray
        Unique entries.
    inverse : np.ma.masked_array, optional
        Integer masked array with the inverse.
    """
    key = np.unique(a, return_inverse=return_inverse)
    if return_inverse:
        key, inverse = key
        barrier = np.argwhere(key.mask)
        if len(barrier) > 0:
            barrier = barrier.squeeze()  # all indices after the barrier have to be shifted (char only?)
            inverse[inverse > barrier] -= 1  # shift everything after the barrier
            if fill_value is None:
                inverse[a.mask.reshape(-1)] = len(key) - 1  # shift masked stuff to the end
            else:
                inverse[a.mask.reshape(-1)] = fill_value
        inverse = np.ma.masked_array(data=inverse, mask=a.mask)
    key = key.data[np.logical_not(key.mask)]
    if return_inverse:
        return key, inverse
    else:
        return key


def cat(*args):
    """
    Concatenate flattened copies of array.

    Parameters
    ----------
    args
        Arrays to concatenate.

    Returns
    -------
    result : np.ndarray
        The concatenated array.
    """
    return np.concatenate(tuple(i.flatten() for i in args))


def lookup(data, key):
    """
    Looks up data in the key and prepares a new array
    with these values.

    Parameters
    ----------
    data : np.ndarray
        The data to lookup.
    key : dict
        The lookup key.

    Returns
    -------
    result : np.ndarray
        The resulting array with the obtained values.
    """
    unique, indices = np.unique(data, return_inverse=True)
    unique = np.array(tuple(key[i] for i in unique))
    return unique[indices]


def dict_reduce(d, operation):
    """
    Reduces dictionary values.

    Parameters
    ----------
    d : Iterable
        Dictionaries to process.
    operation : Callable
        Operation on values.

    Returns
    -------
    result : dict
        A dictionary with reduced values.
    """
    result = {}

    for _d in d:
        for k, v in _d.items():
            if k not in result:
                result[k] = [v]
            else:
                result[k].append(v)

    return {k: operation(v) for k, v in result.items()}


def cartesian(arrays):
    """
    Cartesian product of many coordinate arrays.

    Parameters
    ----------
    arrays : list, tuple
        Samplings along each axis.

    Returns
    -------
    result : np.ndarray
        The resulting coordinates of a many-dimensional grid.
    """
    x = list(i.ravel() for i in np.meshgrid(*arrays))
    return np.stack(x, axis=-1)


def lattice_up_to_radius(radius, lattice_vecs, cover=False, zero=True):
    """
    Computes grid point coordinates of the given lattice.

    Parameters
    ----------
    radius : float
        The cutoff radius.
    lattice_vecs : np.ndarray
        Lattice vectors.
    cover : bool
        If True, adds points to "cover" the grid.
    zero : bool
        If True, includes the origin (otherwise excludes it).

    Returns
    -------
    result : np.ndarray
        Grid points.
    """
    # Compute image count
    k_vecs = np.linalg.inv(lattice_vecs.T)
    k_lengths = np.linalg.norm(k_vecs, axis=-1)
    n_imgs = np.floor(k_lengths * radius).astype(int)

    if cover:
        n_imgs += 1
    combinations = cartesian([np.arange(-n_img, n_img + 1) for n_img in n_imgs])
    drs = combinations @ lattice_vecs
    if cover:
        selection = np.ones(len(drs), dtype=bool)
        for v in cartesian([[-1, 0, 1]] * len(lattice_vecs)):
            selection |= np.linalg.norm(drs + (v @ lattice_vecs)[None, :], axis=-1) < radius
    else:
        selection = np.linalg.norm(drs, axis=-1) < radius
    if not zero:
        selection[len(selection) // 2] = False
    return combinations[selection]


def default_logger(name="main", fmt="[%(levelname)s] %(asctime)s %(message)s", date_fmt="%H:%M:%S",
                   lvl=logging.INFO):
    """
    Prepares a default logger printing to stdout.

    Parameters
    ----------
    name : str
        Logger name.
    fmt : str
        Logger format string.
    date_fmt : str
        Date format.
    lvl : int
        Logger sink level.

    Returns
    -------
    logger
        The resulting logger.
    """
    logger = logging.getLogger(name)
    sink = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter(fmt, date_fmt)
    sink.setLevel(lvl)
    sink.setFormatter(formatter)
    logger.handlers = [sink]
    logger.setLevel(logging.DEBUG)
    return logger


def np_hash(*a):
    """
    Silly implementation of array hashing.

    Parameters
    ----------
    a
        Arrays to hash.

    Returns
    -------
    result : bytes
        The hash value.
    """
    digest = hashlib.sha1()
    for i in a:
        digest.update(np.ascontiguousarray(i).data.tobytes())
    return digest.digest()


def _ro(a: np.ndarray) -> np.ndarray:
    a.flags.writeable = False
    return a


def __assert_same_dtype__(data, *keys, dtype=None):
    for n in keys:
        if data[n] is None:
            continue
        if not isinstance(data[n], torch.Tensor):
            raise ValueError(f"Not a tensor: {n} = {repr(data[n])}")
        if dtype is not None and data[n].dtype != dtype:
            raise ValueError(f"Data type mismatch: {n}.dtype = {data[n].dtype} != {dtype}")
        dtype = data[n].dtype
    return dtype


def __assert_dimension_count__(data, *keys):
    if len(keys) % 2:
        raise RuntimeError("Odd argument count")
    dim_i = keys[1::2]
    keys = keys[::2]
    for n, i in zip(keys, dim_i):
        if n not in data or data[n] is None:
            continue
        if data[n].ndim != i:
            raise ValueError(f"Unexpected dimension count: {n}.ndim = {data[n].ndim:d} != {i:d}")


def __assert_same_dimension__(data, name, *keys, size=None):
    if len(keys) % 2:
        raise RuntimeError("Odd argument count")
    dim_i = keys[1::2]
    keys = keys[::2]
    for n, i in zip(keys, dim_i):
        if n not in data or data[n] is None:
            continue
        if size is not None and data[n].shape[i] != size:
            raise ValueError(f"Inconsistent dimensions: {n}.shape[{i:d}] = {data[n].shape[i]:d} != {name} = {size:d}")
        size = data[n].shape[i]
    return size


def __assert_same_len__(data, *keys):
    l = len(data[keys[0]])
    for k in keys:
        if len(data[k]) != l:
            raise ValueError(f"Inconsistent length: len({k}) = {len(data[k]):d} != {l:d}")
    return l
