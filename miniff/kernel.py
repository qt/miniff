from .potentials import eval_potentials, NestedLocalPotential
from .util import dict_reduce, lattice_up_to_radius, np_hash, _ro, __assert_dimension_count__,\
    __assert_same_dimension__
from .units import load, dump
from ._util import adf as _adf

import numpy as np
from scipy.sparse import coo_matrix, csr_matrix
from scipy.spatial import KDTree

from itertools import product
try:
    from functools import cached_property
except ImportError:
    from .future import cached_property
from functools import wraps
from dataclasses import dataclass, field, InitVar


def encode_species(species, lookup, default=None):
    """
    Transforms species into an array of integers encoding species.

    Parameters
    ----------
    species : list, tuple, np.ndarray
        Species to encode.
    lookup : dict
        A lookup dictionary.
    default : int
        The default value to replace non-existing entries. If None,
        raises KeyError.

    Returns
    -------
    result : np.ndarray
        The resulting integer array.
    """
    result = []
    for i in species:
        try:
            result.append(lookup[i])
        except KeyError:
            if default is not None:
                result.append(default)
            else:
                raise KeyError(f"Could not encode specimen '{i}' with lookup {lookup}. "
                               "Set `default=-1`, for example, to assign unknown species")
    return np.array(result, dtype=np.int32)


def encode_potentials(potentials, lookup, default=None):
    """
    Encodes potentials to have species as integers.

    Parameters
    ----------
    potentials : list, tuple
        Potentials to encode.
    lookup : dict
        A lookup dictionary.
    default : int
        The default value to replace non-existing entries. If None,
        raises KeyError.

    Returns
    -------
    result : list
        The resulting list of potentials.
    """
    result = list(
        i.copy(tag=encode_species(i.tag.split("-"), lookup, default=default) if i.tag is not None else None)
        for i in potentials
    )
    for i in result:
        # TODO: avoid infinite recursion here
        if isinstance(i, NestedLocalPotential):
            i.descriptors = encode_potentials(i.descriptors, lookup, default=default)
    return result


@dataclass(eq=False, frozen=True)
class Cell:
    """Describes a unit cell."""
    vectors: np.ndarray
    coordinates: np.ndarray
    values: np.ndarray
    meta: dict = field(default_factory=dict)
    _vectors_inv: InitVar[np.ndarray] = None

    @cached_property
    def vectors_inv(self) -> np.ndarray:
        return _ro(np.linalg.inv(self.vectors))

    @cached_property
    def cartesian(self) -> np.ndarray:
        return _ro(self.coordinates @ self.vectors)

    @cached_property
    def size(self) -> int:
        return len(self.coordinates)

    @cached_property
    def volume(self) -> float:
        return abs(np.linalg.det(self.vectors))

    @cached_property
    def values_uq(self) -> np.ndarray:
        values_uq, values_encoded = np.unique(self.values, return_inverse=True)
        self.__dict__["values_encoded"] = _ro(values_encoded.astype(np.int32))
        return _ro(values_uq)

    @cached_property
    def values_encoded(self) -> np.ndarray:
        _ = self.values_uq  # trigger species attribute which sets this
        return self.values_encoded

    @cached_property
    def values_lookup(self) -> dict:
        return dict(zip(self.values_uq, np.arange(len(self.values_uq))))

    def __post_init__(self, _vectors_inv):
        d = self.__dict__
        for i in 'vectors', 'coordinates', 'values':
            d[i] = _ro(np.asanyarray(d[i], dtype=float if i != 'values' else None))
        __assert_dimension_count__(d, "vectors", 2, "coordinates", 2, "values", 1)
        __assert_same_dimension__(d, "size", "coordinates", 0, "values", 0)
        __assert_same_dimension__(d, "basis size", "coordinates", 1, "vectors", 0)
        if _vectors_inv is not None:
            d["vectors_inv"] = _vectors_inv

    def __eq__(self, other):
        return isinstance(other, Cell) and np.all(self.vectors == other.vectors) and\
               np.all(self.coordinates == other.coordinates) and np.all(self.values == other.values)

    @classmethod
    def from_cartesian(cls, vectors, cartesian, values, *args, proto=None, _vectors_inv=None, **kwargs):
        """Constructs an instance from cartesian coordinates."""
        if _vectors_inv is None:
            _vectors_inv = np.linalg.inv(vectors)
        if proto is None:
            proto = cls
        return proto(vectors, cartesian @ _vectors_inv, values, *args, _vectors_inv=_vectors_inv, **kwargs)

    def copy(self, vectors=None, coordinates=None, cartesian=None, values=None, meta=None, proto=None):
        """Creates a copy with optional modifications."""
        if coordinates is not None and cartesian is not None:
            raise ValueError("Not more than one of coordinates and cartesian_coordinates is allowed")
        if proto is None:
            proto = self.__class__
        if vectors is not None:
            vinv = None
        else:
            vinv = self.__dict__.get("vectors_inv", None)
        if cartesian is not None:
            return proto.from_cartesian(
                self.vectors.copy() if vectors is None else vectors,
                self.cartesian if cartesian is True else cartesian,
                self.values.copy() if values is None else values,
                self.meta.copy() if meta is None else meta,
                _vectors_inv=vinv,
            )
        return proto(
            self.vectors.copy() if vectors is None else vectors,
            self.coordinates.copy() if coordinates is None else coordinates,
            self.values.copy() if values is None else values,
            self.meta.copy() if meta is None else meta,
            _vectors_inv=vinv,
        )

    def normalized(self):
        """Puts all points inside box boundaries and returns a copy."""
        return self.copy(coordinates=self.coordinates % 1)

    def distances(self, cutoff=None, other=None):
        """
        Computes inter-point distances.

        Parameters
        ----------
        cutoff : float
            Cutoff for obtaining distances.
        other : Cell, np.ndarray
            Other cell to compute distances to

        Returns
        -------
        result : np.ndarray, csr_matrix
            The resulting distance matrix.
        """
        this = self.cartesian
        if other is None:
            other = this
        elif isinstance(other, Cell):
            other = other.cartesian

        if cutoff is None:
            return np.linalg.norm(this[:, np.newaxis] - other[np.newaxis, :], axis=-1)
        else:
            this = cKDTree(this)
            other = cKDTree(other)
            return this.sparse_distance_matrix(other, max_distance=cutoff, )

    def repeated(self, *args):
        """
        Prepares a supercell.

        Parameters
        ----------
        *args
            Repeat counts along each vector.

        Returns
        -------
        supercell : Cell
            The resulting supercell.
        """
        args = np.array(args, dtype=int)
        x = np.prod(args)
        vectors = self.vectors * args[np.newaxis, :]
        coordinates = np.tile(self.coordinates, (x, 1))
        coordinates /= args[np.newaxis, :]
        coordinates.shape = (x, *self.coordinates.shape)
        values = np.tile(self.values, x)
        shifts = list(np.linspace(0, 1, i, endpoint=False) for i in args)
        shifts = np.meshgrid(*shifts)
        shifts = np.stack(shifts, axis=-1)
        shifts = shifts.reshape(-1, shifts.shape[-1])
        coordinates += shifts[:, np.newaxis, :]
        coordinates = coordinates.reshape(-1, coordinates.shape[-1])
        return Cell(vectors, coordinates, values)

    @classmethod
    def from_state_dict(cls, data):
        data = dict(data)
        assert data.pop("type") in ("dfttools.utypes.CrystalCell", "dfttools.types.UnitCell")
        meta = data.pop("meta", None)
        v = data.pop("vectors")
        c = data.pop("coordinates")
        a = data.pop("values")
        b = data.pop("c_basis", None)
        if data:
            raise ValueError(f"Do not recognize additional data: {data}")
        if b == "cartesian":
            return Cell.from_cartesian(v, c, a, meta)
        elif b is None or b == 'cell':
            return Cell(v, c, a, meta)
        else:
            raise ValueError(f"Unknown coordinate basis: {b}")

    def state_dict(self):
        result = dict(
            type="dfttools.utypes.CrystalCell",
            vectors=self.vectors,
            coordinates=self.coordinates,
            values=self.values,
            meta=self.meta,
        )
        return result

    @classmethod
    def load(cls, f):
        """
        Load Cell(s) from stream.

        Parameters
        ----------
        f : file
            File-like object.

        Returns
        -------
        result: list, Cell
            The resulting Cell(s).
        """
        json = load(f)
        squeeze = isinstance(json, dict)
        if squeeze:
            json = [json]
        result = [cls.from_state_dict(i) for i in json]
        if squeeze:
            return result[0]
        else:
            return result

    @staticmethod
    def save(cells, f):
        """
        Saves cells.

        Parameters
        ----------
        cells : list, Cell
            Cells to save.
        f : file
            File-like object.
        """
        if isinstance(cells, (list, tuple, np.ndarray)):
            dump([i.state_dict() for i in cells], f)
        else:
            dump(cells.state_dict(), f)

    @classmethod
    def random(cls, density, atoms, shape=None):
        """
        Prepares a unit cell with random atomic positions.

        Parameters
        ----------
        density : float
            Atomic density.
        atoms : dict
            A dictionary with specimen-count pairs.
        shape : {"box"}
            The shape of the resulting cell.

        Returns
        -------
        result : UnitCell
            The resulting unit cell.
        """
        if shape is None:
            shape = "box"
        n_atoms = sum(atoms.values())
        coords = np.random.rand(n_atoms, 3)
        values = sum(([k] * v for k, v in atoms.items()), [])
        if shape == "box":
            a = (n_atoms / density) ** (1./3)
            return cls(np.eye(3) * a, coords, values)
        else:
            raise ValueError(f"Unknown shape={shape}")


def compute_shift_vectors(cell, cutoff=None, pbc=True):
    """
    Computes shift vectors given a cell and its environment.

    Parameters
    ----------
    cell : Cell
        Cell to process.
    cutoff : float
        Maximal distance computed (smaller=faster).
        Ignored if ``x`` is specified.
    pbc : bool
        If True, assumes periodic boundary conditions.
        Otherwise returns a single zero shift vector.

    Returns
    -------
    shift_vectors : np.ndarray
        A 2D array with shift vectors.
    """
    if pbc:
        shift_vectors = lattice_up_to_radius(cutoff, cell.vectors, cover=True, zero=True)
    else:
        shift_vectors = np.zeros((1, 3))
    return shift_vectors.astype(np.int32)


def compute_reciprocal_grid(cell, cutoff):
    """
    Computes the reciprocal grid.

    Parameters
    ----------
    cell : Cell
        Cell to process.
    cutoff : float
        The value of the reciprocal cutoff.

    Returns
    -------
    grid : np.ndarray
        A 2D array with reciprocal grid points.
    """
    reciprocal_vectors = 2 * np.pi * cell.vectors_inv.T
    return lattice_up_to_radius(cutoff, reciprocal_vectors, zero=False) @ reciprocal_vectors


def compute_images(cell, cutoff, reciprocal_cutoff=None, pbc=True):
    """
    Compute cell images given image shift vectors and
    cutoff distance.

    Parameters
    ----------
    cell : Cell
        Cell to process.
    cutoff : float
        The distance cutoff value.
    reciprocal_cutoff : float
        Optional reciprocal cutoff for the reciprocal grid.
    pbc : bool
        If True, assumes periodic boundary conditions.

    Returns
    -------
    images : CellImages
        Images with neighbor and distance information.
    """
    shift_vectors = compute_shift_vectors(cell, cutoff, pbc=pbc)
    # Create a super-cell with neighbors
    cartesian_row = cell.cartesian
    cartesian_col = (cell.cartesian[np.newaxis, :, :] + (shift_vectors[:, np.newaxis, :] @ cell.vectors)).reshape(-1, 3)

    # Collect close neighbors
    t_row = KDTree(cartesian_row)
    t_col = KDTree(cartesian_col)

    spd = t_row.sparse_distance_matrix(t_col, cutoff, output_type="coo_matrix")

    # Get rid of the diagonal
    mask = spd.row + len(shift_vectors) // 2 * len(cartesian_row) != spd.col
    sparse_pair_distances = coo_matrix((spd.data[mask], (spd.row[mask], spd.col[mask])), shape=spd.shape).tocsr()
    shift = np.repeat(shift_vectors[:, np.newaxis, :], cell.size, 1).reshape(-1, 3)

    if reciprocal_cutoff is not None:
        reciprocal_grid = compute_reciprocal_grid(cell, reciprocal_cutoff)
    else:
        reciprocal_grid = None
    return CellImages(cell=cell, cartesian=cartesian_col, shift=shift, distances=sparse_pair_distances,
                      cutoff=cutoff, reciprocal_grid=reciprocal_grid)


def eval(images, potentials, kname, squeeze=True, ignore_missing_species=False, out=None, **kwargs):
    """
    Computes potentials: values, gradients and more.

    Parameters
    ----------
    images : CellImages
        Cell and its images.
    potentials : list, LocalPotential
        A list of potentials or a single potential.
    kname : str, None
        Function to evaluate: 'kernel', 'kernel_gradient' or whatever
        other kernel function set for all potentials in the list.
    squeeze : bool
        If True, returns a single array whenever a single potential
        is passed.
    ignore_missing_species : bool
        If True, no error is raised whenever a specimen in the
        potential description is not found in the cell.
    out : np.ndarray
        The output buffer `[n_potentials, n_atoms]` for
        kname == "kernel" and `[n_potentials, n_atoms, n_atoms, 3]`
        for kname == "kernel_gradient". Any kind of reduction including
        `resolved=False` and calls `self.total`, `self.grad` calls will
        use the buffer for intermediate results but will still allocate
        a new array for the output.
    kwargs
        Other arguments to `eval_potentials`.

    Returns
    -------
    result : np.ndarray
        The result of the potential computation given the cell data.
    """
    sole = not isinstance(potentials, (list, tuple))
    if sole:
        potentials = potentials,
    cell = images.cell

    potentials = list(potentials)
    encoded_potentials = encode_potentials(potentials, cell.values_lookup,
                                           default=-1 if ignore_missing_species else None)

    additional_inputs = dict(volume=cell.volume)
    if "charges" in cell.meta:
        additional_inputs["charges"] = np.array(cell.meta["charges"], dtype=float)
    if images.reciprocal_grid is not None:
        additional_inputs["k_grid"] = images.reciprocal_grid
    out = eval_potentials(encoded_potentials, kname, images.distances, cell.cartesian,
                          images.cartesian, images.shift, cell.values_encoded, pre_compute_r=False,
                          additional_inputs=additional_inputs, cutoff=images.cutoff, out=out, **kwargs)
    if sole and squeeze:
        return out[0]
    else:
        return out


def total(images, potentials, kname="kernel", squeeze=False, resolving=False, **kwargs):
    """
    Total energy as a sum of all possible potential terms.

    Note that this function totally ignores any symmetry issues related to
    double-counting, etc.

    Parameters
    ----------
    images : CellImages
        Cell and its images.
    potentials : list, LocalPotential
        A list of potentials or a single potential.
    kname : str, None
        Function to evaluate: 'kernel', 'kernel_gradient' or whatever
        other kernel function set for all potentials in the list.
    squeeze : bool
        If True, returns a single array whenever a single potential
        is passed.
    resolving : bool
        If True, runs species-resolving kernels.
    kwargs
        Other arguments to `eval`.

    Returns
    -------
    energy : float
        The total energy value.
    """
    return eval(images, potentials, kname, squeeze=squeeze, resolving=resolving, **kwargs).sum(axis=0)


def grad(images, potentials, kname="kernel_gradient", **kwargs):
    """
    Total energy gradients with respect to cartesian atomic
    coordinates.

    Similarly to `self.total`, this function totally ignores
    any symmetry issues related to double-counting, etc.

    Parameters
    ----------
    images : CellImages
        Cell and its images.
    potentials : list, LocalPotential
        A list of potentials or a single potential.
    kname : str, None
        Function to evaluate: 'kernel', 'kernel_gradient' or whatever
        other kernel function set for all potentials in the list.
    kwargs
        Other arguments to `total`.

    Returns
    -------
    gradients : np.ndarray
        Total energy gradients.
    """
    return total(images, potentials, kname=kname, **kwargs)


def grad_cell(images, potentials, kname="kernel_cell_gradient", **kwargs):
    """
    Total energy gradients with respect to cell vectors assuming
    cartesian atomic coordinates are fixed.

    Parameters
    ----------
    images : CellImages
        Cell and its images.
    potentials : list, LocalPotential
        A list of potentials or a single potential.
    kname : str, None
        Function to evaluate: 'kernel', 'kernel_gradient' or whatever
        other kernel function set for all potentials in the list.
    kwargs
        Other arguments to `total`.

    Returns
    -------
    gradients : np.ndarray
        Total energy gradients.
    """
    return total(images, potentials, kname=kname, **kwargs)


def common_cutoff(potentials):
    """
    The maximal (common) cutoff of many potentials.

    Parameters
    ----------
    potentials : Iterable
        Potentials to compute the cutoff for.

    Returns
    -------
    result : float
        The resulting cutoff.
    """
    if len(potentials) == 0:
        return 0
    else:
        return max(i.cutoff for i in potentials)


@dataclass(eq=False, frozen=True)
class CellImages:
    cell: Cell
    cartesian: np.ndarray
    shift: np.ndarray
    distances: csr_matrix
    cutoff: float
    reciprocal_grid: np.ndarray = None

    @cached_property
    def size(self):
        return len(self.cartesian)

    @cached_property
    def n_images(self):
        return self.size / self.cell.size

    def pair_reduction_function(self, f, fmt="{}-{}"):
        """
        Pair reduction function.

        Parameters
        ----------
        f : Callable
            A function reducing pair-specific distances,
            see `self.rdf` for an example.
        fmt : str
            A format string for keys.

        Returns
        -------
        result : dict
            Pair function values.
        """
        result = {}
        distances = self.distances.tocoo()
        pair_id = self.cell.values_encoded[distances.row] * len(self.cell.values_uq) + self.cell.values_encoded[
            distances.col % distances.shape[0]]
        for i_s1, s1 in enumerate(self.cell.values_uq):
            for i_s2, s2 in enumerate(self.cell.values_uq[i_s1:]):
                i_s2 += i_s1
                k = fmt.format(s1, s2)
                pid = i_s1 * len(self.cell.values_uq) + i_s2
                mask = pair_id == pid
                spd = distances.data[mask]
                val = f(spd, i_s1, i_s2)
                if val is not None:
                    result[k] = val
        return result

    def rdf(self, r, sigma, fmt="{}-{}"):
        """
        Computes the radial distribution function.

        Parameters
        ----------
        r : np.ndarray, float
            Radius values.
        sigma : float
            Smearing.
        fmt : str
            A format string for keys.

        Returns
        -------
        result : dict
            Radial distribution function values.
        """
        if not isinstance(r, np.ndarray):
            r = np.array([r], dtype=float)
            squeeze = True
        else:
            squeeze = False
        factor = 1 / sigma / (2 * np.pi) ** .5

        def f(spd, row, col):
            weights = np.exp(- (spd[:, np.newaxis] - r[np.newaxis, :]) ** 2 / 2 / sigma ** 2).sum(axis=0)
            return weights * factor / 4 / np.pi / r ** 2 / (self.cell.values_encoded == row).sum()

        result = self.pair_reduction_function(f, fmt=fmt)
        if squeeze:
            return {k: v[0] for k, v in result.items()}
        else:
            return result

    def adf(self, theta, sigma, cutoff, fmt="{}-[{},{}]"):
        """
        Computes the angular distribution function.

        Parameters
        ----------
        theta : np.ndarray, float
            Radius values.
        sigma : float
            Smearing.
        cutoff : float
            Radial cutoff value.
        fmt : str
            A format string for keys.

        Returns
        -------
        result : dict
            Radial distribution function values.
        """
        if not isinstance(theta, np.ndarray):
            r = np.array([theta], dtype=float)
            squeeze = True
        else:
            squeeze = False
        factor = 1 / sigma / (2 * np.pi) ** .5
        result = {}

        for i_s1, s1 in enumerate(self.cell.values_uq):
            for i_s2, s2 in enumerate(self.cell.values_uq):
                for i_s3, s3 in enumerate(self.cell.values_uq[i_s2:]):
                    i_s3 += i_s2
                    k = fmt.format(s1, s2, s3)

                    out = result[k] = np.zeros_like(theta)
                    _adf(
                        self.distances.indptr,
                        self.distances.indices,
                        self.distances.data,
                        self.cell.cartesian,
                        self.cartesian,
                        cutoff, theta, sigma,
                        self.cell.values_encoded,
                        np.array([i_s1, i_s2, i_s3], dtype=np.int32),
                        out,
                    )
                    out *= factor
        if squeeze:
            return {k: v[0] for k, v in result.items()}
        else:
            return result

    eval = eval
    total = total
    grad = grad
    grad_cell = grad_cell


class SnapshotHistory(list):
    def __call__(self, cell):
        if len(self) and self[-1] == cell:
            return
        self.append(cell)


def _parameters_cache(f):
    cache = {}

    @wraps(f)
    def _lru(self, *a):
        h = np_hash(*a)
        if h in cache:
            return cache[h]
        else:
            cache.clear()
            result = f(self, *a)
            cache[h] = result
            return result

    return _lru


class ScalarFunctionWrapper:
    def __init__(self, sample, potentials, include_coordinates=True, include_vectors=False, normalize=None,
                 prefer_parallel=None, cell_logger=None, track_potential_fidelity=False, **kwargs):
        """
        A wrapper providing interfaces to total energy and gradient computation.

        Parameters
        ----------
        sample : Cell
            The sample Cell.
        potentials : list, LocalPotential
            Potentials defining the total energy value.
        include_coordinates : bool
            If True, includes coordinates into parameters.
        include_vectors : bool
            If True, includes vectors into parameters.
        normalize : bool
            Normalizes the cell at each step if True.
        prefer_parallel : bool
            A flag to prefer parallel potential computations.
        cell_logger : Callable
            A function accumulating intermediate cell objects.
        track_potential_fidelity : bool
            If True, computes potential fidelity and stores it
            as a part of the history recorded.
        kwargs
            Additional arguments to ``compute_images``.
        """
        if normalize is None:
            normalize = kwargs.get("pbc", True)
        self.sample = sample
        self.potentials = potentials
        self.include_coordinates = include_coordinates
        self.include_vectors = include_vectors
        self.normalize = normalize
        self.prefer_parallel = prefer_parallel
        self.cell_logger = cell_logger
        self.track_potential_fidelity = track_potential_fidelity
        if track_potential_fidelity:  # early alert
            for i in potentials:
                try:
                    i.get_kernel_by_name("fidelity")
                except KeyError:
                    raise ValueError("One or more potentials do not include the 'fidelity' kernel")
        self.compute_images_kwargs = {"cutoff": common_cutoff(potentials), **kwargs}

    def start_recording(self):
        """Starts recording of coordinates passed."""
        self.cell_logger = SnapshotHistory()

    def stop_recording(self) -> SnapshotHistory:
        """Stops the recording of coordinates and returns all
        cells recorded."""
        result, self.cell_logger = self.cell_logger, None
        return result

    def make_cell(self, coordinates, vectors):
        return self.sample.copy(coordinates=coordinates, vectors=vectors,
                                meta={k: v for k, v in self.sample.meta.items() if k not in ("forces", "_stress")})

    @_parameters_cache
    def eval_(self, coordinates, vectors):
        """
        Computes function and gradients.
        (Skips saving into history.)

        Parameters
        ----------
        coordinates : np.ndarray
            Cell (crystal) coordinates.
        vectors : np.ndarray
            Cell vectors.

        Returns
        -------
        cell : Cell
            The resulting cell with energy and gradients set.
        f : float
            The energy value.
        gc : np.ndarray
            Cartesian gradients.
        gv : np.ndarray
            Vector gradients.
        """
        cell = self.make_cell(coordinates, vectors)
        if self.normalize:
            cell = cell.normalized()
        images = compute_images(cell, **self.compute_images_kwargs)
        f = cell.meta["total-energy"] = total(images, self.potentials, prefer_parallel=self.prefer_parallel)

        if self.include_coordinates:
            gc = grad(images, self.potentials, prefer_parallel=self.prefer_parallel)
            cell.meta['forces'] = - gc
        else:
            gc = None

        if self.include_vectors:
            gv = grad_cell(images, self.potentials, prefer_parallel=self.prefer_parallel)
            cell.meta['_stress'] = - gv
        else:
            gv = None

        if self.track_potential_fidelity:
            images.cell.meta["fidelity"] = total(images, self.potentials, "fidelity", resolving=True,
                                                 prefer_parallel=self.prefer_parallel)
        return images.cell, f, gc, gv

    def eval(self, coordinates, vectors):
        """
        Computes function and gradients.

        Parameters
        ----------
        coordinates : np.ndarray
            Cell (crystal) coordinates.
        vectors : np.ndarray
            Cell vectors.

        Returns
        -------
        cell : Cell
            The resulting cell with energy and gradients set.
        f : float
            The energy value.
        gc : np.ndarray
            Cartesian gradients.
        gv : np.ndarray
            Vector gradients.
        """
        cell, f, gc, gv = self.eval_(coordinates, vectors)
        self._notify(cell)
        return cell, f, gc, gv

    def _notify(self, cell):
        """Notifies of a new call."""
        if self.cell_logger is not None:
            self.cell_logger(cell)

    def eval_to_cell(self, coordinates: np.ndarray, vectors: np.ndarray) -> Cell:
        return self.eval(coordinates, vectors)[0]

    def f(self, coordinates: np.ndarray, vectors: np.ndarray) -> float:
        return self.eval(coordinates, vectors)[1]

    def gc(self, coordinates: np.ndarray, vectors: np.ndarray) -> np.ndarray:
        return self.eval(coordinates, vectors)[2]

    def gv(self, coordinates: np.ndarray, vectors: np.ndarray) -> np.ndarray:
        return self.eval(coordinates, vectors)[3]


def batch_rdf(cells, *args, inner=CellImages.rdf, **kwargs):
    """
    Averaged radial distribution function.

    Parameters
    ----------
    cells : list, tuple
        A collection of wrapped cells to process.
    inner : Callable
        The function computing distribution for a single cell.
    args
    kwargs
        Arguments to ``inner``.

    Returns
    -------
    result : dict
        Radial distribution function values.
    """
    def mean(x):
        return sum(x) / len(x)
    return dict_reduce((inner(w, *args, **kwargs) for w in cells), mean)


def profile(potentials, f, *args, **kwargs):
    """
    Profiles a collection of potentials.

    Parameters
    ----------
    potentials : list
        Potentials to profile.
    f : Callable
        A function `f(x1, ...) -> UnitCell` preparing a unit cell
        for the given set of parameters.
    args
        Sampling of `x1`, ... arguments of the function `f`.
    kwargs
        Arguments to ``compute_images``.

    Returns
    -------
    energy : np.ndarray
        Energies on the multidimensional grid defined by `args`.
    """
    if not isinstance(potentials, (list, tuple)):
        potentials = potentials,

    result = np.empty(tuple(len(i) for i in args), dtype=float)
    result_shape = result.shape
    result.shape = result.size,

    cutoff = common_cutoff(potentials)

    for i, pt in enumerate(product(*args)):
        cell = f(*pt)
        images = compute_images(cell, cutoff, **kwargs)
        result[i] = total(images, potentials, ignore_missing_species=True)

    result.shape = result_shape
    return result


def profile_strain(potentials, cell, *args, **kwargs):
    """
    Profiles a collection of potentials by applying strain.

    Parameters
    ----------
    potentials : list
        Potentials to profile.
    cell : UnitCell
        The original cell.
    args
        Relative strains along all vectors.
    kwargs
        Arguments to ``compute_images``.

    Returns
    -------
    energy : np.ndarray
        Energies of strained cells.
    """
    def _f(*_strain):
        _s = np.ones(len(cell.vectors))
        _s[:len(_strain)] = _strain
        return cell.copy(vectors=cell.vectors * _s[:, np.newaxis])

    return profile(potentials, _f, *args, **kwargs)


def profile_directed_strain(potentials, cell, strain, direction, **kwargs):
    """
    Profiles a collection of potentials by applying strain.

    Parameters
    ----------
    potentials : list
        Potentials to profile.
    cell : UnitCell
        The original cell.
    strain : Iterable
        The relative strain.
    direction : list, tuple, np.ndarray
        The strain direction.
    kwargs
        Arguments to ``compute_images``.

    Returns
    -------
    energy : np.ndarray
        Energies of strained cells.
    """
    direction = np.array(direction)

    def _f(_strain):
        return cell.copy(vectors=cell.vectors * (direction * _strain + (1 - direction))[:, np.newaxis])

    return profile(potentials, _f, strain, **kwargs)
