from .util import unit_vectors, lookup, split2, cat
from .kernel import Cell, ScalarFunctionWrapper, compute_images
from .potentials import harmonic_repulsion_potential_family
from .units import element_masses

from scipy.integrate import solve_ivp
from scipy.optimize import minimize
import numpy as np

from warnings import warn
from inspect import getfullargspec


class DynamicsWarning(UserWarning):
    pass


class AbstractDynamics:
    def __init__(self, wrapper: ScalarFunctionWrapper, t0: float = 0):
        self.wrapper = wrapper
        self.t = t0
        self.p = np.empty(2 * self.size, dtype=float)
        self.history = []

    @classmethod
    def from_cell(cls, cell: Cell, potentials: list, **kwargs):
        # arguments to dynamics.__init__
        spec = getfullargspec(cls)
        cls_init = dict(zip(spec.args[-len(spec.defaults):], spec.defaults))
        cls_init = {k: kwargs.pop(k) if k in kwargs else v for k, v in cls_init.items()}
        return cls(ScalarFunctionWrapper(cell, potentials, **kwargs), **cls_init)

    @property
    def sample(self) -> Cell:
        return self.wrapper.sample

    @property
    def state(self) -> (np.ndarray, float):
        return self.p.copy(), self.t

    @state.setter
    def state(self, v: tuple):
        self.p, self.t = v

    def push(self):
        """Saves the current state"""
        self.history.append(self.state)

    def pop(self):
        """Restores the previous state"""
        self.state = self.history.pop()

    def __enter__(self):
        self.push()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.pop()

    # --------------------
    # Internal coordinates
    # --------------------
    @property
    def size(self):
        """The number of internal coordinates."""
        raise NotImplementedError

    @property
    def x(self) -> np.ndarray:
        """Internal coordinates"""
        return self.p[:self.size]

    @x.setter
    def x(self, v: np.ndarray):
        self.p[:self.size] = v

    @property
    def x_dot(self) -> np.ndarray:
        """Internal velocities"""
        return self.p[self.size:]

    @x_dot.setter
    def x_dot(self, v: np.ndarray):
        self.p[self.size:] = v

    # ------
    # Output
    # ------
    def to_cell_parameters(self, x: np.ndarray) -> (np.ndarray, np.ndarray):
        """Unpacks internal coordinates into cell coordinates and vectors"""
        raise NotImplementedError

    def interpret(self, x: np.ndarray, x_dot: np.ndarray = None, t: float = None) -> Cell:
        """Turns internal coordinates and time into Cell"""
        # x_dot ignored
        result = self.wrapper.eval_to_cell(*self.to_cell_parameters(x))
        if t is not None:
            result.meta['time'] = t
        return result

    # ---------
    # Equations
    # ---------
    def potential_energy(self, x: np.ndarray, _ptr=1):
        """Potential energy expression"""
        return self.wrapper.eval(*self.to_cell_parameters(x))[_ptr]

    def potential_grad(self, p: np.ndarray) -> np.ndarray:
        """Potential energy gradients"""
        raise NotImplementedError

    def ek(self, p: np.ndarray) -> np.ndarray:
        """Kinetic energy of particles"""
        raise NotImplementedError

    def velocity_for_ek(self, ek: np.ndarray, x0: np.ndarray) -> np.ndarray:
        """
        Finds velocities to match the target kinetic energy.

        Parameters
        ----------
        ek : np.ndarray
            Target kinetic energies.
        x0 : np.ndarray
            Starting velocities.

        Returns
        -------
        result : np.ndarray
            The resulting scaled velocities.
        """
        coords, coords_dot = split2(x0)

        def loss(x: np.ndarray) -> float:
            return ((self.ek(cat(coords, x)) - ek) ** 2).sum() ** .5
        result = minimize(loss, x0=coords_dot, tol=1e-10)
        assert result.success
        return cat(coords, result.x)

    def eom(self, t: float, p: np.ndarray) -> np.ndarray:
        """Equations of motion"""
        raise NotImplementedError


def match_cartesian_dot_to_ek(ek, velocities, masses, metric=None):
    """
    Matches the cartesian velocity to the given kinetic energy.

    Parameters
    ----------
    ek : np.ndarray
        Target kinetic energies.
    velocities : np.ndarray
        Initial velocities.
    masses : np.ndarray
        Masses.
    metric : np.ndarray
        Optional metric.

    Returns
    -------
    result : np.ndarray
        The resulting velocities.
    """
    velocities = unit_vectors(velocities, metric=metric)
    velocities *= (2 * ek[:, None] / masses) ** .5
    return velocities


class FixedCellDynamics(AbstractDynamics):
    def __init__(self, wrapper, masses="atomic", x0=None, v0=None, t0=0):
        """
        Dynamics of cartesian coordinates with a fixed unit cell.

        Parameters
        ----------
        wrapper : ScalarFunctionWrapper
            The wrapper.
        masses : {np.ndarray, dict, str}
            Particle masses.
        x0 : np.ndarray
            Starting coordinates.
        v0 : np.ndarray
            Starting velocities.
        t0 : float
            Starting time.
        """
        if wrapper.include_coordinates is not True:
            raise ValueError(f"Wrapper does not define cartesian gradients")
        super().__init__(wrapper, t0)

        if not isinstance(masses, np.ndarray):
            masses = np.repeat(
                lookup_masses(masses, wrapper.sample.values)[:, None],
                self.sample.cartesian.shape[-1], 1)
        assert masses.shape == self.sample.cartesian.shape
        self.masses = masses

        if x0 is None:  # init to sample coordinates
            x0 = wrapper.sample.cartesian
        self.x = x0.flatten()

        if v0 is None:  # init to zero velocity
            v0 = np.zeros_like(wrapper.sample.cartesian)
        self.x_dot = v0.flatten()

    @classmethod
    def from_cell(cls, cell: Cell, potentials: list, include_coordinates=True, include_vectors=False, **kwargs):
        args = locals().copy()
        args.update(args.pop("kwargs"))
        del args["cls"], args["__class__"]
        return super().from_cell(**args)

    # --------------------
    # Internal coordinates
    # --------------------
    @property
    def size(self):
        return self.wrapper.sample.cartesian.size

    # ------
    # Output
    # ------
    def to_cell_parameters(self, x: np.ndarray) -> (np.ndarray, np.ndarray):
        return x.reshape(*self.sample.cartesian.shape) @ self.sample.vectors_inv, self.sample.vectors

    def interpret(self, x: np.ndarray, x_dot: np.ndarray = None,
                  t: float = None, _v="cartesian_dot", _s="cartesian", _ek="ek") -> Cell:
        result = super().interpret(x, t=t)
        if x_dot is not None:
            result.meta[_v] = x_dot.reshape(*getattr(result, _s).shape)
            result.meta[_ek] = self.ek(cat(x, x_dot))
        return result

    # ---------
    # Equations
    # ---------
    def potential_grad(self, p: np.ndarray, _ptr=2) -> np.ndarray:
        return self.potential_energy(p, _ptr).flatten()

    def ek(self, p: np.ndarray) -> np.ndarray:
        coords, velocities = split2(p)
        velocities = velocities.reshape(*self.masses.shape)
        return (self.masses * velocities ** 2).sum(axis=-1) / 2

    def velocity_for_ek(self, ek: np.ndarray, p: np.ndarray) -> np.ndarray:
        _, velocities = split2(p)
        velocities.shape = self.masses.shape
        velocities = match_cartesian_dot_to_ek(ek, velocities, self.masses)
        return cat(_, velocities)

    def eom(self, t: float, p: np.ndarray) -> np.ndarray:
        coords, velocities = split2(p)
        return cat(velocities, - self.potential_grad(coords) / self.masses.flatten())


class CellOnlyDynamics(FixedCellDynamics):
    def __init__(self, wrapper, masses=None, x0=None, v0=None, t0=0):
        """
        Dynamics of cell vectors with crystal coordinates fixed.

        Parameters
        ----------
        wrapper : ScalarFunctionWrapper
            The wrapper.
        masses : np.ndarray
            Effective cell vector masses.
        x0 : np.ndarray
            Starting vectors.
        v0 : np.ndarray
            Starting cell vector velocities.
        t0 : float
            Starting time.
        """
        if wrapper.include_vectors is not True:
            raise ValueError(f"Wrapper does not define vectors gradients")
        AbstractDynamics.__init__(self, wrapper, t0)

        if masses is None:
            masses = np.ones_like(self.sample.vectors)
        assert masses.shape == self.sample.vectors.shape
        self.masses = masses

        if x0 is None:  # init to sample vectors
            x0 = wrapper.sample.vectors
        self.x = x0.flatten()

        if v0 is None:  # init to zero velocity
            v0 = np.zeros_like(wrapper.sample.vectors)
        self.x_dot = v0.flatten()

    @classmethod
    def from_cell(cls, cell: Cell, potentials: list, include_coordinates=False, include_vectors=True,
                  pbc=True, **kwargs):
        args = locals().copy()
        args.update(args.pop("kwargs"))
        del args["cls"], args["__class__"]
        return super().from_cell(**args)

    # --------------------
    # Internal coordinates
    # --------------------
    @property
    def size(self):
        return self.wrapper.sample.vectors.size

    # ------
    # Output
    # ------
    def to_cell_parameters(self, x: np.ndarray) -> (np.ndarray, np.ndarray):
        return self.sample.coordinates, x.reshape(*self.sample.vectors.shape)

    def interpret(self, x: np.ndarray, x_dot: np.ndarray = None,
                  t: float = None, _v="vectors_dot", _s="vectors", _ek="_ek-vectors") -> Cell:
        return super().interpret(x, x_dot, t, _v=_v, _s=_s, _ek=_ek)

    # ---------
    # Equations
    # ---------
    def potential_grad(self, p: np.ndarray, _ptr=3) -> np.ndarray:
        return super().potential_grad(p, _ptr=_ptr)


class PRDynamics(AbstractDynamics):
    def __init__(self, wrapper, masses="atomic", masses_v=None, x0=None, v0=None, x0_v=None, v0_v=None, t0=0):
        """
        Parrinello-Rahman dynamics defined by the Lagrangian
        L = 1/2 m (ṡ · a) · (a' · ṡ') + 1/2 W ȧ · ȧ' - E

        s - crystal coordinates
        a - crystal vectors
        m - atomic masses
        W - effective cell masses
        Note that there are no cross-terms: thus, this expression
        does not reduce to cartesian velocities.

        With metric G = a · a' equations of motion are:
        ∂L / ∂ṡ = m ṡ · G
        ∂L / ∂ȧ = W ȧ
        ∂L / ∂s = - ∂E / ∂s
        ∂L / ∂a = ṡ ⊗ (ṡ · a) - ∂E / ∂a

        s̈ = - ∂E/∂s · G⁻¹ / m - ṡ · 2 (ȧ · a') · G⁻¹
        W ä = ṡ ⊗ (ṡ · a) - ∂E/∂a

        Parameters
        ----------
        wrapper : ScalarFunctionWrapper
            The wrapper.
        masses : {np.ndarray, dict, str}
            Particle masses.
        masses_v : np.ndarray
            Effective cell vector masses.
        x0 : np.ndarray
            Starting cell coordinates.
        v0 : np.ndarray
            Starting velocities.
        x0_v: np.ndarray
            Starting vectors.
        v0_v : np.ndarray
            Starting cell vector velocities.
        t0 : float
            Starting time.
        """
        if wrapper.include_coordinates is not True:
            raise ValueError(f"Wrapper does not define cartesian gradients")
        if wrapper.include_vectors is not True:
            raise ValueError(f"Wrapper does not define vectors gradients")
        AbstractDynamics.__init__(self, wrapper, t0)

        if not isinstance(masses, np.ndarray):
            masses = np.repeat(
                lookup_masses(masses, wrapper.sample.values)[:, None],
                self.sample.cartesian.shape[-1], 1)
        assert masses.shape == self.sample.coordinates.shape
        self.masses = masses

        if masses_v is None:
            masses_v = np.full_like(self.sample.vectors, default_fictitious_cell_mass(masses))
        assert masses_v.shape == self.sample.vectors.shape
        self.masses_v = masses_v

        if x0 is None:  # init to sample coordinates
            x0 = wrapper.sample.coordinates
        if x0_v is None:  # init to sample vectors
            x0_v = wrapper.sample.vectors
        self.x = cat(x0, x0_v)

        if v0 is None:  # init to zero velocity
            v0 = np.zeros_like(wrapper.sample.coordinates)
        if v0_v is None:  # init to zero vector velocity
            v0_v = np.zeros_like(wrapper.sample.vectors)
        self.x_dot = cat(v0, v0_v)

    @classmethod
    def from_cell(cls, cell: Cell, potentials: list, include_coordinates=True, include_vectors=True,
                  pbc=True, **kwargs):
        args = locals().copy()
        args.update(args.pop("kwargs"))
        del args["cls"], args["__class__"]
        return super().from_cell(**args)

    # --------------------
    # Internal coordinates
    # --------------------
    @property
    def size(self):
        return self.wrapper.sample.coordinates.size + self.wrapper.sample.vectors.size

    # ------
    # Output
    # ------
    def to_cell_parameters(self, x: np.ndarray) -> (np.ndarray, np.ndarray):
        return split2(x, (self.sample.coordinates, self.sample.vectors))

    def interpret(self, x: np.ndarray, x_dot: np.ndarray = None, t: float = None) -> Cell:
        result = super().interpret(x, x_dot, t)
        if x_dot is not None:
            v_c, v_v = self.to_cell_parameters(x_dot)
            result.meta["coordinates_dot"] = v_c
            result.meta["vectors_dot"] = v_v
            ek = self.ek(cat(x, x_dot))
            result.meta["ek"] = ek[:len(self.sample.coordinates)]
            result.meta["_ek-vectors"] = ek[len(self.sample.coordinates):]
        return result

    # ---------
    # Equations
    # ---------
    def _potential_grad(self, p: np.ndarray) -> (np.ndarray, np.ndarray):
        coordinates, vectors = self.to_cell_parameters(p)
        _, _, cartesian_g, vectors_g = self.wrapper.eval(coordinates, vectors)
        # a coordinate change here: E(cartesian, vectors) -> E(crystal @ vectors, vectors)
        crystal_g = cartesian_g @ vectors.T  # crystal gradients
        vectors_g_new = np.einsum("...ai,aj->...ji", cartesian_g, coordinates) + vectors_g
        return crystal_g, vectors_g_new

    def potential_grad(self, p: np.ndarray, _ptr=2) -> np.ndarray:
        return cat(*self._potential_grad(p))

    def ek(self, p: np.ndarray) -> np.ndarray:
        coords, velocities = split2(p)
        crystal, vectors = split2(coords, (self.sample.coordinates, self.sample.vectors))
        crystal_dot, vectors_dot = split2(velocities, (self.sample.coordinates, self.sample.vectors))
        cartesian_dot = crystal_dot @ vectors
        return cat((cartesian_dot ** 2 * self.masses).sum(axis=-1), (vectors_dot ** 2 * self.masses_v).sum(axis=-1)) / 2

    def velocity_for_ek(self, ek: np.ndarray, p: np.ndarray) -> np.ndarray:
        coords, velocities = split2(p)
        crystal, vectors = split2(coords, (self.sample.coordinates, self.sample.vectors))
        crystal_dot, vectors_dot = split2(velocities, (self.sample.coordinates, self.sample.vectors))
        cartesian_ek, vectors_ek = ek[:len(crystal)], ek[len(crystal):]
        metric = vectors @ vectors.T

        crystal_dot = match_cartesian_dot_to_ek(cartesian_ek, crystal_dot, self.masses, metric)
        vectors_dot = match_cartesian_dot_to_ek(vectors_ek, vectors_dot, self.masses_v)
        return cat(crystal, vectors, crystal_dot, vectors_dot)

    def eom(self, t: float, p: np.ndarray) -> np.ndarray:
        coords, velocities = split2(p)
        s, a = self.to_cell_parameters(coords)
        s_dot, a_dot = self.to_cell_parameters(velocities)
        s_grad, a_grad = self._potential_grad(coords)

        G = a @ a.T
        G_inv = np.linalg.inv(G)

        # s̈ = - ∂E/∂s · G⁻¹ / m - ṡ · 2 (ȧ · a') · G⁻¹
        s_ddot = - s_grad @ G_inv / self.masses - 2 * s_dot @ (a_dot @ a.T @ G_inv)

        # W ä = ṡ ⊗ (ṡ · a) - ∂E/∂a
        # ij <- ai,ak,kj -- note implicit sum over atoms "a"
        a_ddot = (s_dot.T @ s_dot @ a - a_grad) / self.masses_v
        return cat(velocities, s_ddot, a_ddot)


def relax(dynamics, driver=minimize, update_state=False, snapshots=False, **kwargs):
    """
    Finds the local minimum of the base cell.

    Parameters
    ----------
    dynamics : AbstractDynamics
        The dynamics to relax.
    driver : Callable
        The minimizer.
    update_state : bool
        If True, updates the state of ``dynamics`` at the end.
    snapshots : bool
        If True, returns snapshots.
    kwargs
        Keyword arguments to ``driver``.

    Returns
    -------
    result : np.ndarray
        The resulting array of parameters.
    snapshots : list, optional
        Snapshots if requested.
    """
    if snapshots:
        dynamics.wrapper.start_recording()
    result = driver(
        dynamics.potential_energy,
        x0=dynamics.x,
        jac=dynamics.potential_grad,
        **kwargs
    )
    if not result.success:
        warn(str(result.message), DynamicsWarning)
    if update_state:
        dynamics.x = result.x
    result_x = result.x
    result_p = cat(result_x, dynamics.x_dot)
    if snapshots:
        snapshots = dynamics.wrapper.stop_recording()
        snapshots.append(dynamics.interpret(result_x, t=dynamics.t))
        return result_p, snapshots
    else:
        return result_p


def integrate(dynamics, time, driver=solve_ivp, update_state=False, snapshots=False, **kwargs):
    """
    Integrates equation of motion up to the desired time.

    Parameters
    ----------
    dynamics : AbstractDynamics
        The dynamics to integrate.
    time : float
        Integration time.
    driver
        Solver driver.
    update_state : bool
        If True, updates the state of ``dynamics`` at the end.
    snapshots : bool
        If True, returns snapshots.
    kwargs
        Arguments to the driver.

    Returns
    -------
    result : np.ndarray
        The resulting array of parameters.
    snapshots : list, optional
        Snapshots if requested.
    """
    result_ = driver(dynamics.eom, (dynamics.t, dynamics.t + time), dynamics.p, **kwargs)
    result = result_.y[:, -1]
    result_t = result_.t[-1]
    if update_state:
        dynamics.p = result
        dynamics.t = result_t
    if snapshots:
        snapshots = [
            dynamics.interpret(*split2(gc), t)
            for gc, t in zip(result_.y.T, result_.t)
        ]
        snapshots.append(dynamics.interpret(*split2(result), result_t))
        return result, snapshots
    else:
        return result


def nvt_vs(dynamics, ek, dt, alpha, ek_tolerance=0, update_state=False, snapshots=False, **kwargs):
    """
    Constant-temperature dynamics via velocity scaling.
    Implemented as in OpenMX.
    http://openmx-square.org/openmx_man3.9/node60.html

    Parameters
    ----------
    dynamics : AbstractDynamics
        The dynamics to evolve.
    ek : {float, Iterable}
        Target kinetic energy.
    dt : Iterable
        Time periods between rescaling.
    alpha : float
        Rescaling strength 0: no rescaling, 1: hard rescaling.
    ek_tolerance : float
        The allowed deviation of the kinetic energy.
    update_state : bool
        If True, updates the state of ``dynamics`` at the end.
    snapshots : bool
        If True, saves snapshots.
    kwargs
        Other arguments to ``integrate``.

    Returns
    -------
    result : np.ndarray
        The resulting array of parameters.
    snapshots : list, optional
        Snapshots if requested.
    """
    assert 0 <= alpha <= 1
    if snapshots:
        snapshots = []
    else:
        snapshots = None
    if isinstance(ek, (list, tuple)):
        ek = np.asanyarray(ek)
    else:
        ek = np.full(dynamics.ek(dynamics.p).shape, ek)
    with dynamics:
        for _dt in dt:

            # velocity scaling
            ek_actual = dynamics.ek(dynamics.p)  # actual kinetic energy
            to_rescale = np.logical_or(np.abs(ek_actual - ek) > ek_tolerance, ek_actual == 0)  # update mask
            ek_actual[to_rescale] = ek_actual[to_rescale] * (1 - alpha) + ek[to_rescale] * alpha  # target kinetic energy
            dynamics.p = dynamics.velocity_for_ek(ek_actual, dynamics.p)
            assert np.all(np.isfinite(dynamics.p)), f"Failed to rescale x0={dynamics.p}"

            # evolution
            p = integrate(dynamics, _dt, update_state=True, **kwargs)

            if snapshots is not None:
                snapshots.append(dynamics.interpret(dynamics.x, dynamics.x_dot, dynamics.t))
        if update_state:
            state = dynamics.state
    if update_state:
        dynamics.state = state
    if snapshots is not None:
        return p, snapshots
    else:
        return p


for_name = {"fixed-cell": FixedCellDynamics, "cell-only": CellOnlyDynamics, "pr": PRDynamics}


def default_fictitious_cell_mass(m):
    """
    Default choice for fictitious cell masses.

    Parameters
    ----------
    m : np.ndarray
        Atomic masses to derive from.

    Returns
    -------
    result : float
        Resulting cell mass.
    """
    return .75 * sum(m) / np.pi ** 2


def lookup_masses(masses, values):
    """
    Looks up masses.

    Parameters
    ----------
    masses : {dict, str}
        Dictionary to lookup from.
    values : np.ndarray
        Species.

    Returns
    -------
    result : np.ndarray
        Per-specimen masses.
    """
    if masses is None:
        return np.ones(values.shape)
    if masses == 'atomic':
        masses = element_masses()
    elif isinstance(masses, dict):
        pass
    else:
        raise ValueError(f"Unknown masses: {masses}")
    return lookup(values, masses)


def relax_cell(cell, potentials, rtn_history=False, normalize=True, prefer_parallel=None,
               driver=minimize, pbc=True, vc=False, **kwargs):
    """
    Finds the local minimum of the base cell.

    Parameters
    ----------
    cell : Cell
        Cell to relax.
    potentials : list
        Potential to use.
    rtn_history : bool
        If True, returns intermediate atomic configurations.
    normalize : bool
        If True, normalizes the cell at each step.
    prefer_parallel : bool
        A flag to prefer parallel potential computations.
    driver : Callable
        The minimizer.
    pbc : bool
        Periodic boundary conditions.
    vc : bool
        Variable-cell relaxation.
    kwargs
        Keyword arguments to `scipy.optimize.minimize`.

    Returns
    -------
    cell : Cell, list
        Either final unit cell or a history of all unit cells during the relaxation.
    """
    dynamics = {False: FixedCellDynamics, True: PRDynamics}[vc]
    wrapper = dynamics.from_cell(cell, potentials, normalize=normalize, prefer_parallel=prefer_parallel,
                                 pbc=pbc, include_vectors=vc, masses=None)
    _, snapshots = relax(wrapper, driver=driver, snapshots=True, **kwargs)
    if rtn_history:
        return snapshots
    else:
        return snapshots[-1]


def random_cell(density, atoms, a, shape=None, warn_eps=1e-2):
    """
    Prepares a unit cell with random atomic positions
    relaxed down to a specific interatomic distance.

    Parameters
    ----------
    density : float
        Atomic density.
    atoms : dict
        A dictionary with specimen-count pairs.
    a : float
        Target minimal distance.
    shape : {"box"}
        The shape of the resulting cell.
    warn_eps : float
        Specifies the maximal deviation from the target interatomic distance
        when a warning is raised.

    Returns
    -------
    result : UnitCell
        The resulting unit cell.
    """
    result = relax_cell(
        Cell.random(density, {"x": sum(atoms.values())}, shape=shape),
        [harmonic_repulsion_potential_family(a=a, epsilon=1)],
    )
    d = compute_images(result, a * (1 - warn_eps)).distances.data
    if len(d) > 0:
        warn(f"Could not achieve target interatomic distance {a:.3e} with the actual value of {d.min():.3e}",
             DynamicsWarning)
    return result.copy(values=sum(([k] * v for k, v in atoms.items()), []))
