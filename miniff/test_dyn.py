from . import kernel, potentials, dyn, util, units

import numpy as np
from numpy import testing
from unittest import TestCase


def _test_energy_conservation(samples, e0, _ek="ek"):
    for i in samples:
        testing.assert_allclose(i.meta[_ek].sum() + i.meta['total-energy'], e0, atol=1e-4)


def _plot(history):
    from matplotlib import pyplot
    pyplot.figure(figsize=(8, 5))
    def _take(key, attr="meta"):
        return np.array([getattr(i, attr)[key] for i in history])
    t = _take("time") if "time" in history[0].meta else np.arange(len(history))
    def _plot(key, attr="meta", **kwargs):
        pyplot.plot(t, _take(key, attr), label=f"{attr}[{repr(key)}]", marker="o", **kwargs)
    _plot("total-energy")
    for i in range(history[0].size):
        _plot((i, 0), "cartesian")
    for i in range(history[0].size):
        _plot((i, 0), "coordinates", ls="--")
    _plot((0, 0), "vectors")
    if "_stress" in history[0].meta:
        pyplot.plot(t, np.array([i.meta["_stress"][0, 0] for i in history]), label="stress[0, 0]")
    pyplot.legend()
    pyplot.grid()
    pyplot.show()


class UpdateTestMixin:
    def test_update_relax(self):
        orig = self.dyn.p.copy()
        orig_t = self.dyn.t
        with self.dyn:
            result = dyn.relax(self.dyn, update_state=True)
            testing.assert_allclose(result, self.dyn.p)
        testing.assert_allclose(orig, self.dyn.p)
        testing.assert_allclose(orig_t, self.dyn.t)

    def test_update_integrate(self):
        orig = self.dyn.p.copy()
        orig_t = self.dyn.t
        with self.dyn:
            result = dyn.integrate(self.dyn, self.t / 8, update_state=True)
            testing.assert_allclose(result, self.dyn.p)
        testing.assert_allclose(orig, self.dyn.p)
        testing.assert_allclose(orig_t, self.dyn.t)

    def test_update_nvt_vs(self):
        orig = self.dyn.p.copy()
        orig_t = self.dyn.t
        with self.dyn:
            result = dyn.nvt_vs(self.dyn, self.e0_, [self.t / 8] * 2, 1, update_state=True)
            testing.assert_allclose(result, self.dyn.p)
        testing.assert_allclose(orig, self.dyn.p)
        testing.assert_allclose(orig_t, self.dyn.t)


class MoleculeTests(UpdateTestMixin, TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        # artificial well potential with minimum at (x=1, e=-2)
        cls.p = p = [
            potentials.harmonic_repulsion_potential_family(a=2, epsilon=8),  # repulsion
            potentials.harmonic_repulsion_potential_family(a=3, epsilon=-9),  # attraction
        ]
        cls.k = sum(i.parameters["epsilon"] / i.parameters["a"] ** 2 * 2 for i in p)
        cls.m = 1
        cls.omega = (2 * cls.k / cls.m) ** .5
        cls.t = np.pi * 2 / cls.omega
        cls.d = d = 0.8
        cls.v = d * cls.omega / 2
        cls.e0_ = cls.k / 2 * (d ** 2)
        cls.e0 = cls.e0_ - 2
        cls.f0 = cls.k * d
        cls.t0 = t0 = 3.1415
        cls.dyn = dyn.FixedCellDynamics.from_cell(
            kernel.Cell(np.diag((1, 1, 1)), [[0., 0., 0.], [d + 1, 0., 0.]], ["H", "H"]),
            p,
            pbc=False,
            masses=np.full((2, 3), cls.m),
            v0=np.zeros((2, 3)),
            t0=t0
        )

    def test_defaults(self):
        dynamics = dyn.FixedCellDynamics.from_cell(self.dyn.sample, self.dyn.wrapper.potentials)

        testing.assert_equal(dynamics.wrapper.include_coordinates, True)
        testing.assert_equal(dynamics.wrapper.include_vectors, False)

        testing.assert_equal(dynamics.x, dynamics.sample.cartesian.flatten())
        testing.assert_equal(dynamics.x_dot, np.zeros(6))
        testing.assert_equal(dynamics.masses, np.full((2, 3), units.element_masses()['H']))

    def test_x0(self):
        testing.assert_allclose(self.dyn.x, [0, 0, 0, 1 + self.d, 0, 0])
        testing.assert_allclose(self.dyn.x_dot, [0, 0, 0, 0, 0, 0])
        testing.assert_allclose(self.dyn.p, [0, 0, 0, 1 + self.d, 0, 0, 0, 0, 0, 0, 0, 0])
        testing.assert_allclose(self.dyn.potential_energy(self.dyn.x), self.e0)
        testing.assert_allclose(self.dyn.potential_grad(self.dyn.x), [- self.f0, 0, 0, self.f0, 0, 0])

    def test_numgrad(self):
        testing.assert_allclose(util.num_grad(self.dyn.potential_energy, self.dyn.x, eps=1e-7),
                                self.dyn.potential_grad(self.dyn.x))

    def test_numgrad_random(self):
        x = np.array([.3, .1, .4, .1, .15, .19])
        testing.assert_allclose(util.num_grad(self.dyn.potential_energy, x, eps=1e-7), self.dyn.potential_grad(x))

    def test_relax(self):
        p, snapshots = dyn.relax(self.dyn, snapshots=True)
        result = snapshots[-1]
        testing.assert_allclose(result.cartesian[1] - result.cartesian[0], [1, 0, 0])
        testing.assert_allclose(result.meta['total-energy'], -2)
        testing.assert_allclose(result.meta['forces'], 0, atol=1e-6)
        testing.assert_allclose(result.meta['time'], self.t0)
        return result

    def test_dyn(self, _k_x_dot='cartesian_dot'):
        """One oscillation period"""
        p, snapshots = dyn.integrate(self.dyn, self.t, rtol=1e-8, snapshots=True)
        result = snapshots[-1]
        _test_energy_conservation(snapshots, self.e0)
        testing.assert_allclose(result.cartesian, self.dyn.sample.cartesian, atol=1e-4)
        testing.assert_allclose(result.meta['total-energy'], self.e0, atol=1e-5)
        testing.assert_allclose(result.meta['forces'], [[self.f0, 0, 0], [-self.f0, 0, 0]], atol=1e-4)
        testing.assert_allclose(result.meta[_k_x_dot], 0, atol=1e-5)
        testing.assert_allclose(result.meta['time'], self.t + self.t0)
        return result

    def test_trivial_nvt_vs(self, _k_x_dot='cartesian_dot'):
        """Four oscillation periods"""
        times = np.full(4, self.t)
        timestamps = np.cumsum(times) + self.t0
        p, snapshots = dyn.nvt_vs(self.dyn, 0, times, 1, snapshots=True, rtol=1e-8)
        for i, t_expected in zip(snapshots, timestamps):
            testing.assert_allclose(i.cartesian, self.dyn.sample.cartesian, atol=1e-4)
            testing.assert_allclose(i.meta['total-energy'], self.e0, atol=1e-4)
            testing.assert_allclose(i.meta['forces'], [[self.f0, 0, 0], [-self.f0, 0, 0]], atol=1e-4)
            testing.assert_allclose(i.meta[_k_x_dot], 0, atol=1e-4)
            testing.assert_allclose(i.meta['time'], t_expected)
        return snapshots

    def test_trivial_nvt_vs2(self, _k_x_dot='cartesian_dot', _vecs=False):
        """Four half-oscillation periods"""
        with self.dyn:
            times = np.full(4, self.t / 2)
            timestamps = np.cumsum(times) + self.t0
            x0, v0 = np.array([[self.d / 2, 0, 0], [1 + self.d / 2, 0, 0]]), np.array([[self.v, 0, 0], [-self.v, 0, 0]])
            if _vecs:
                p0 = util.cat(x0, self.dyn.sample.vectors, v0, self.dyn.sample.vectors)
            else:
                p0 = util.cat(x0, v0)
            self.dyn.p = p0
            p, snapshots = dyn.nvt_vs(self.dyn,
                                self.e0_ / 2 if not _vecs else [self.e0_ / 2] * 2 + [0] * 3,
                                times, 1, snapshots=True, rtol=1e-8)
            for i_i, (i, t_expected) in enumerate(zip(snapshots, timestamps)):
                phase = 2 * (i_i % 2) - 1
                testing.assert_allclose(i.cartesian, x0, atol=1e-4)
                testing.assert_allclose(i.meta['total-energy'], -2, atol=1e-4)  # at minimum
                testing.assert_allclose(i.meta['forces'], [[0, 0, 0], [0, 0, 0]], atol=1e-4)
                testing.assert_allclose(i.meta[_k_x_dot], v0 * phase, atol=1e-4)
                testing.assert_allclose(i.meta['time'], t_expected)
        return snapshots

    def test_damping_nvt_vs(self, _k_x_dot='cartesian_dot'):
        """Full damping at period/8"""
        times = np.full(4, self.t / 8)
        timestamps = np.cumsum(times) + self.t0
        p, snapshots = dyn.nvt_vs(self.dyn, 0, times, 1, snapshots=True, rtol=1e-8)
        for i_i, (i, t_expected) in enumerate(zip(snapshots, timestamps)):
            d = i.cartesian[1, 0] - i.cartesian[0, 0] - 1
            f1 = .5 ** ((i_i + 1) / 2)
            f2 = .5 ** (i_i + 1)
            testing.assert_allclose(d, self.d * f1, atol=1e-4)
            testing.assert_allclose(i.meta['total-energy'], (self.e0 + 2) * f2 - 2, atol=1e-4)
            testing.assert_allclose(i.meta['forces'], [[self.f0 * f1, 0, 0], [-self.f0 * f1, 0, 0]], atol=1e-4)
            testing.assert_allclose(i.meta[_k_x_dot], [[self.v * f1, 0, 0], [- self.v * f1, 0, 0]], atol=1e-4)
            testing.assert_allclose(i.meta['time'], t_expected)
        return snapshots


class FixedCellTests(UpdateTestMixin, TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.p = p = potentials.harmonic_repulsion_potential_family(a=2, epsilon=1)
        cls.k = p.parameters["epsilon"] / p.parameters["a"] ** 2 * 2
        cls.m = 1
        cls.omega = (4 * cls.k / cls.m) ** .5
        cls.t = np.pi * 2 / cls.omega
        cls.d = d = 0.2
        cls.v = d * cls.omega / 2
        cls.e0 = cls.e0_ = cls.k / 2 * (1.2 ** 2 + 0.8 ** 2)
        cls.f0 = cls.k * 0.4
        cls.t0 = t0 = 3.1415
        cls.dyn = dyn.FixedCellDynamics.from_cell(
            kernel.Cell(np.diag((2., 10., 10.)), [[0., 0., 0.], [.5 + d / 2, 0., 0.]], ["H", "H"]),
            [p],
            masses=np.full((2, 3), cls.m),
            v0=np.zeros((2, 3)),
            t0=t0
        )

    def test_defaults(self):
        dynamics = dyn.FixedCellDynamics.from_cell(self.dyn.sample, self.dyn.wrapper.potentials)

        testing.assert_equal(dynamics.wrapper.include_coordinates, True)
        testing.assert_equal(dynamics.wrapper.include_vectors, False)

        testing.assert_equal(dynamics.x, dynamics.sample.cartesian.flatten())
        testing.assert_equal(dynamics.x_dot, np.zeros(6))
        testing.assert_equal(dynamics.masses, np.full((2, 3), units.element_masses()['H']))

    def test_x0(self):
        testing.assert_allclose(self.dyn.x, [0, 0, 0, 1.2, 0, 0])
        testing.assert_allclose(self.dyn.x_dot, [0, 0, 0, 0, 0, 0])
        testing.assert_allclose(self.dyn.p, [0, 0, 0, 1.2, 0, 0, 0, 0, 0, 0, 0, 0])
        testing.assert_allclose(self.dyn.potential_energy(self.dyn.x), self.e0)
        testing.assert_allclose(self.dyn.potential_grad(self.dyn.x), [-self.f0, 0, 0, self.f0, 0, 0])

    def test_numgrad(self):
        testing.assert_allclose(util.num_grad(self.dyn.potential_energy, self.dyn.x, eps=1e-7),
                                self.dyn.potential_grad(self.dyn.x))

    def test_numgrad_random(self):
        x = np.array([.3, .1, .4, .1, .15, .19])
        testing.assert_allclose(util.num_grad(self.dyn.potential_energy, x, eps=1e-7), self.dyn.potential_grad(x))

    def test_relax(self, _total_energy=1):
        p, snapshots = dyn.relax(self.dyn, snapshots=True)
        result = snapshots[-1]
        testing.assert_allclose(result.coordinates.sum(axis=0), [0.6, 0, 0])
        testing.assert_allclose(result.meta['total-energy'], _total_energy * self.k)
        testing.assert_allclose(result.meta['forces'], 0, atol=1e-8)
        testing.assert_allclose(result.meta['time'], self.t0)
        return result

    def test_dyn(self, _k_x_dot="cartesian_dot"):
        p, snapshots = dyn.integrate(self.dyn, self.t, rtol=1e-8, snapshots=True)
        _test_energy_conservation(snapshots, self.e0)
        result = snapshots[-1]
        testing.assert_allclose(result.cartesian, self.dyn.sample.cartesian, atol=1e-4)
        testing.assert_allclose(result.meta['total-energy'], self.e0, atol=1e-5)
        testing.assert_allclose(result.meta['forces'], [[self.f0, 0, 0], [-self.f0, 0, 0]], atol=1e-4)
        testing.assert_allclose(result.meta[_k_x_dot], 0, atol=1e-5)
        testing.assert_allclose(result.meta['time'], self.t + self.t0)
        return result

    def test_trivial_nvt_vs(self, _k_x_dot="cartesian_dot"):
        times = np.full(4, self.t)
        timestamps = np.cumsum(times) + self.t0
        p, snapshots = dyn.nvt_vs(self.dyn, 0, times, 1, snapshots=True, rtol=1e-8)
        for i, t_expected in zip(snapshots, timestamps):
            testing.assert_allclose(i.cartesian, self.dyn.sample.cartesian, atol=1e-4)
            testing.assert_allclose(i.meta['total-energy'], self.e0, atol=1e-5)
            testing.assert_allclose(i.meta['forces'], [[self.f0, 0, 0], [-self.f0, 0, 0]], atol=1e-4)
            testing.assert_allclose(i.meta[_k_x_dot], 0, atol=1e-5)
            testing.assert_allclose(i.meta['time'], t_expected)
        return snapshots

    def test_trivial_nvt_vs2(self, _k_x_dot='cartesian_dot', _vecs=False, _cartesian=True):
        """Four half-oscillation periods"""
        with self.dyn:
            times = np.full(4, self.t / 2)
            timestamps = np.cumsum(times) + self.t0
            x0, v0 = np.array([[self.d / 2, 0, 0], [1 + self.d / 2, 0, 0]]), np.array([[self.v, 0, 0], [-self.v, 0, 0]])
            x0_ = x0 / 2
            v0_ = v0 / 2
            if _vecs:
                p0 = util.cat(x0_, self.dyn.sample.vectors, v0_, self.dyn.sample.vectors)
            else:
                p0 = util.cat(x0, v0)
            self.dyn.p = p0
            p, snapshots = dyn.nvt_vs(self.dyn, (self.e0 - self.k) / 2, times, 1, snapshots=True, rtol=1e-8)
            for i_i, (i, t_expected) in enumerate(zip(snapshots, timestamps)):
                phase = 2 * (i_i % 2) - 1
                testing.assert_allclose(i.cartesian, x0, atol=1e-4)
                testing.assert_allclose(i.meta['total-energy'], self.k, atol=1e-4)  # at minimum
                testing.assert_allclose(i.meta['forces'], [[0, 0, 0], [0, 0, 0]], atol=1e-4)
                testing.assert_allclose(i.meta[_k_x_dot], {"cartesian_dot": v0, "coordinates_dot": v0_}[_k_x_dot] * phase, atol=1e-4)
                testing.assert_allclose(i.meta['time'], t_expected)
        return snapshots

    def test_damping_nvt_vs(self, _k_x_dot="cartesian_dot", _f_x_dot=1):
        times = np.full(4, self.t / 8)
        timestamps = np.cumsum(times) + self.t0
        p, snapshots = dyn.nvt_vs(self.dyn, 0, times, 1, snapshots=True, rtol=1e-8)
        for i_i, (i, t_expected) in enumerate(zip(snapshots, timestamps)):
            d = i.cartesian[1, 0] - i.cartesian[0, 0] - 1
            f1 = .5 ** ((i_i + 1) / 2)
            f2 = .5 ** (i_i + 1)
            testing.assert_allclose(d, self.d * f1, atol=1e-4)
            testing.assert_allclose(i.meta['total-energy'], (self.e0 - .5) * f2 + .5, atol=1e-5)
            testing.assert_allclose(i.meta['forces'], [[self.f0 * f1, 0, 0], [-self.f0 * f1, 0, 0]], atol=1e-4)
            testing.assert_allclose(i.meta[_k_x_dot], _f_x_dot * np.array([[self.v * f1, 0, 0], [- self.v * f1, 0, 0]]), atol=1e-5)
            testing.assert_allclose(i.meta['time'], t_expected)
        return snapshots


class CellOnlyTests(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.p = p = potentials.harmonic_repulsion_potential_family(a=2, epsilon=1)
        cls.k = p.parameters["epsilon"] / p.parameters["a"] ** 2 * 2
        cls.m = 1
        cls.omega = (cls.k / cls.m) ** .5
        cls.t = np.pi * 2 / cls.omega
        cls.e0 = cls.k / 2
        cls.f0 = cls.k * 1
        cls.v = (2 * cls.e0 / cls.m) ** .5
        cls.dyn = dyn.CellOnlyDynamics.from_cell(
            kernel.Cell(np.diag((1., 10., 10.)), [[0., 0., 0.]], ["H"]),
            [p],
            masses=np.full((3, 3), cls.m),
            v0=np.zeros((3, 3))
        )

    def test_defaults(self):
        dynamics = dyn.CellOnlyDynamics.from_cell(self.dyn.sample, self.dyn.wrapper.potentials)

        testing.assert_equal(dynamics.wrapper.include_coordinates, False)
        testing.assert_equal(dynamics.wrapper.include_vectors, True)

        testing.assert_equal(dynamics.x, self.dyn.sample.vectors.flatten())
        testing.assert_equal(dynamics.x_dot, np.zeros(9))
        testing.assert_equal(dynamics.masses, np.ones((3, 3)))

    def test_x0(self):
        testing.assert_allclose(self.dyn.x, [1, 0, 0, 0, 10, 0, 0, 0, 10])
        testing.assert_allclose(self.dyn.x_dot, [0] * 9)
        testing.assert_allclose(self.dyn.p, [1, 0, 0, 0, 10, 0, 0, 0, 10] + [0] * 9)
        testing.assert_allclose(self.dyn.potential_energy(self.dyn.x), self.e0)
        testing.assert_allclose(self.dyn.potential_grad(self.dyn.x), [-self.f0, 0, 0, 0, 0, 0, 0, 0, 0])

    def test_numgrad(self):
        testing.assert_allclose(util.num_grad(self.dyn.potential_energy, self.dyn.x, eps=1e-7),
                                self.dyn.potential_grad(self.dyn.x))

    def test_numgrad_random(self):
        x = np.array([.3, .1, .4, .1, .5, .9, .2, .6, .7])
        testing.assert_allclose(util.num_grad(self.dyn.potential_energy, x, eps=1e-7), self.dyn.potential_grad(x))

    def test_relax(self):
        p, snapshots = dyn.relax(self.dyn, snapshots=True)
        result = snapshots[-1]
        testing.assert_allclose(result.vectors, [
            [2, 0, 0],
            [0, 10, 0],
            [0, 0, 10],
        ])
        testing.assert_allclose(result.meta['total-energy'], 0)
        testing.assert_allclose(result.meta['_stress'], 0, atol=1e-8)
        testing.assert_allclose(result.meta['time'], 0)
        return result

    def test_dyn(self):
        p, snapshots = dyn.integrate(self.dyn, self.t / 4, rtol=1e-8, snapshots=True)
        _test_energy_conservation(snapshots, self.e0, _ek="_ek-vectors")
        result = snapshots[-1]
        testing.assert_allclose(result.vectors, [
            [2, 0, 0],
            [0, 10, 0],
            [0, 0, 10],
        ], atol=1e-4)
        testing.assert_allclose(result.meta['total-energy'], 0, atol=1e-5)
        testing.assert_allclose(result.meta['_stress'], 0, atol=1e-5)
        testing.assert_allclose(result.meta['vectors_dot'], [
            [self.v, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
        ], atol=1e-5)
        testing.assert_allclose(result.meta['time'], self.t / 4)
        return result

    def test_damping_nvt_vs(self):
        times = np.full(4, self.t / 8)
        timestamps = np.cumsum(times)
        p, snapshots = dyn.nvt_vs(self.dyn, 0, times, 1, snapshots=True, rtol=1e-8)
        for i_i, (i, t_expected) in enumerate(zip(snapshots, timestamps)):
            f1 = .5 ** ((i_i + 1) / 2)
            f2 = .5 ** (i_i + 1)
            testing.assert_allclose(i.cartesian, self.dyn.sample.cartesian, atol=1e-4)
            testing.assert_allclose(i.meta['total-energy'], self.e0 * f2, atol=1e-5)
            testing.assert_allclose(i.meta['_stress'], [
                [self.f0 * f1, 0, 0],
                [0, 0, 0],
                [0, 0, 0],
            ], atol=1e-4)
            testing.assert_allclose(i.meta['vectors_dot'], [
                [self.v * f1, 0, 0],
                [0, 0, 0],
                [0, 0, 0],
            ], atol=1e-5)
            testing.assert_equal(i.meta["time"], t_expected)
        return snapshots


class PRMoleculeTests(MoleculeTests):
    @classmethod
    def setUpClass(cls) -> None:
        super(PRMoleculeTests, cls).setUpClass()
        s = cls.dyn.sample
        cls.dyn = dyn.PRDynamics.from_cell(
            cls.dyn.sample,
            cls.dyn.wrapper.potentials,
            pbc=False,
            masses=np.full(s.coordinates.shape, cls.m),
            masses_v=np.full(s.vectors.shape, 1e8),  # very massive cell
            v0=np.zeros_like(s.coordinates),
            v0_v=np.zeros_like(s.vectors),
            t0=cls.dyn.t,
        )

    def test_defaults(self):
        dynamics = dyn.PRDynamics.from_cell(self.dyn.sample, self.dyn.wrapper.potentials)
        m = units.element_masses()['H']

        testing.assert_equal(dynamics.wrapper.include_coordinates, True)
        testing.assert_equal(dynamics.wrapper.include_vectors, True)
        testing.assert_equal(dynamics.wrapper.compute_images_kwargs["pbc"], True)

        testing.assert_equal(dynamics.x, util.cat(self.dyn.sample.coordinates, self.dyn.sample.vectors))
        testing.assert_equal(dynamics.x_dot, np.zeros(15))
        testing.assert_equal(dynamics.p, util.cat(self.dyn.sample.coordinates, self.dyn.sample.vectors, np.zeros(15)))
        testing.assert_equal(dynamics.masses, np.full((2, 3), m))
        testing.assert_equal(dynamics.masses_v, np.full((3, 3), m * 1.5 / np.pi ** 2))

    def test_x0(self):
        testing.assert_allclose(self.dyn.x, [0, 0, 0, 1 + self.d, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1])
        testing.assert_allclose(self.dyn.x_dot, [0] * 15)
        testing.assert_allclose(self.dyn.p, [0, 0, 0, 1 + self.d, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1] + [0] * 15)
        testing.assert_allclose(self.dyn.potential_energy(self.dyn.x), self.e0)
        testing.assert_allclose(self.dyn.potential_grad(self.dyn.x), [- self.f0, 0, 0, self.f0, 0, 0,
                                                                              self.f0 * (1 + self.d)] + [0] * 8)

    def test_numgrad(self):
        testing.assert_allclose(util.num_grad(self.dyn.potential_energy, self.dyn.x, eps=1e-7),
                                self.dyn.potential_grad(self.dyn.x))

    def test_numgrad_random(self):
        x = np.array([.3, .1, .4, .1, .5, .9, 1, .2, .3, .5, 1, .4, .7, 0, .8])
        testing.assert_allclose(util.num_grad(self.dyn.potential_energy, x, eps=1e-7), self.dyn.potential_grad(x))

    def test_relax(self):
        result = super().test_relax()
        testing.assert_allclose(result.meta['_stress'], 0, atol=1e-8)

    def test_dyn(self, _k_x_dot="coordinates_dot"):
        result = super().test_dyn(_k_x_dot=_k_x_dot)
        testing.assert_allclose(result.meta['_stress'], -self.dyn.wrapper.eval(
            *self.dyn.to_cell_parameters(self.dyn.x))[3], atol=1e-6)
        testing.assert_allclose(result.meta['vectors_dot'], 0, atol=1e-5)

    def test_damping_nvt_vs(self, _k_x_dot='coordinates_dot'):
        super().test_damping_nvt_vs(_k_x_dot=_k_x_dot)

    def test_trivial_nvt_vs2(self, _k_x_dot='coordinates_dot', _vecs=True):
        super().test_trivial_nvt_vs2(_k_x_dot=_k_x_dot, _vecs=_vecs)

    def test_trivial_nvt_vs(self, _k_x_dot='coordinates_dot'):
        super().test_trivial_nvt_vs(_k_x_dot=_k_x_dot)


class PRFixedCellTests(FixedCellTests):
    @classmethod
    def setUpClass(cls) -> None:
        super(PRFixedCellTests, cls).setUpClass()
        s = cls.dyn.sample
        cls.dyn = dyn.PRDynamics.from_cell(
            s, cls.dyn.wrapper.potentials,
            masses=np.full(s.coordinates.shape, cls.m),
            masses_v=np.full(s.vectors.shape, 1e8),  # very massive cell
            v0=np.zeros_like(s.coordinates),
            v0_v=np.zeros_like(s.vectors),
            t0=cls.dyn.t,
        )

    def test_defaults(self):
        dynamics = dyn.PRDynamics.from_cell(self.dyn.sample, self.dyn.wrapper.potentials)
        m = units.element_masses()['H']
        testing.assert_equal(dynamics.x, util.cat(self.dyn.sample.coordinates, self.dyn.sample.vectors))
        testing.assert_equal(dynamics.x_dot, np.zeros(15))
        testing.assert_equal(dynamics.p, util.cat(self.dyn.sample.coordinates, self.dyn.sample.vectors, np.zeros(15)))
        testing.assert_equal(dynamics.masses, np.full((2, 3), m))
        testing.assert_equal(dynamics.masses_v, np.full((3, 3), m * 1.5 / np.pi ** 2))

    def test_x0(self):
        x0 = [0, 0, 0, (1 + self.d) / 2, 0, 0, 2, 0, 0, 0, 10, 0, 0, 0, 10]
        testing.assert_allclose(self.dyn.x, x0)
        testing.assert_allclose(self.dyn.x_dot, [0] * 15)
        testing.assert_allclose(self.dyn.p, x0 + [0] * 15)
        testing.assert_allclose(self.dyn.potential_energy(self.dyn.x), self.e0)
        testing.assert_allclose(self.dyn.potential_grad(self.dyn.x), [- self.f0 * 2, 0, 0, self.f0 * 2, 0, 0,
                                                                      - 0.96 * self.k] + [0] * 8)

    def test_numgrad(self):
        testing.assert_allclose(util.num_grad(self.dyn.potential_energy, self.dyn.x, eps=1e-7),
                                self.dyn.potential_grad(self.dyn.x))

    def test_numgrad_random(self):
        x = np.array([.3, .1, .4, .1, .5, .9, 1, .2, .3, .5, 1, .4, .7, 0, .8])
        testing.assert_allclose(util.num_grad(self.dyn.potential_energy, x, eps=1e-7), self.dyn.potential_grad(x))

    def test_relax(self):
        result = super().test_relax(_total_energy=0)
        testing.assert_allclose(result.meta['_stress'], 0, atol=1e-8)
        return result

    def test_dyn(self, _k_x_dot="coordinates_dot"):
        result = super().test_dyn(_k_x_dot=_k_x_dot)
        testing.assert_allclose(result.meta['_stress'], -self.dyn.wrapper.eval(
            *self.dyn.to_cell_parameters(self.dyn.x))[3], atol=1e-5)
        testing.assert_allclose(result.meta['vectors_dot'], 0, atol=1e-5)

    def test_trivial_nvt_vs(self, _k_x_dot='coordinates_dot'):
        super().test_trivial_nvt_vs(_k_x_dot=_k_x_dot)

    def test_trivial_nvt_vs2(self, _k_x_dot='coordinates_dot', _vecs=True):
        super().test_trivial_nvt_vs2(_k_x_dot=_k_x_dot, _vecs=_vecs)

    def test_damping_nvt_vs(self, _k_x_dot='coordinates_dot', _f_x_dot=.5):
        super().test_damping_nvt_vs(_k_x_dot=_k_x_dot, _f_x_dot=_f_x_dot)


class PRCellOnlyTests(CellOnlyTests):
    @classmethod
    def setUpClass(cls) -> None:
        super(PRCellOnlyTests, cls).setUpClass()
        s = cls.dyn.wrapper.sample
        cls.dyn = dyn.PRDynamics.from_cell(
            s, cls.dyn.wrapper.potentials,
            masses=np.ones_like(s.coordinates),
            masses_v=np.full(s.vectors.shape, cls.m),
            v0=np.zeros_like(s.coordinates),
            v0_v=np.zeros_like(s.vectors),
        )

    def test_defaults(self):
        dynamics = dyn.PRDynamics.from_cell(self.dyn.sample, self.dyn.wrapper.potentials)
        m = units.element_masses()['H']

        testing.assert_equal(dynamics.x, util.cat(self.dyn.sample.coordinates, self.dyn.sample.vectors))
        testing.assert_equal(dynamics.x_dot, np.zeros(12))
        testing.assert_equal(dynamics.masses, np.full((1, 3), m))
        testing.assert_equal(dynamics.masses_v, np.full((3, 3), m * .75 / np.pi ** 2))

    def test_x0(self):
        testing.assert_allclose(self.dyn.x, [0, 0, 0, 1, 0, 0, 0, 10, 0, 0, 0, 10])
        testing.assert_allclose(self.dyn.x_dot, [0] * 12)
        testing.assert_allclose(self.dyn.p, [0, 0, 0, 1, 0, 0, 0, 10, 0, 0, 0, 10] + [0] * 12)
        testing.assert_allclose(self.dyn.potential_energy(self.dyn.x), self.e0)
        testing.assert_allclose(self.dyn.potential_grad(self.dyn.x), [0, 0, 0, -self.f0, 0, 0, 0, 0, 0, 0, 0, 0])

    def test_numgrad(self):
        testing.assert_allclose(util.num_grad(self.dyn.potential_energy, self.dyn.x, eps=1e-7),
                                self.dyn.potential_grad(self.dyn.x))

    def test_numgrad_random(self):
        x = np.array([.3, .1, .4, 1, .2, .3, .5, 1, .4, .7, 0, .8])
        testing.assert_allclose(util.num_grad(self.dyn.potential_energy, x, eps=1e-7), self.dyn.potential_grad(x))

    def test_relax(self):
        result = super().test_relax()
        testing.assert_allclose(result.meta['forces'], 0, atol=1e-8)

    def test_dyn(self):
        result = super().test_dyn()
        testing.assert_allclose(result.meta['forces'], 0, atol=1e-8)

    def test_damping_nvt_vs(self):
        result = super().test_damping_nvt_vs()
        for i in result:
            testing.assert_allclose(i.meta['forces'], 0, atol=1e-8)


def test_pr_free_dynamics_vecs():
    """Free dynamics vectors only"""
    cell = kernel.Cell(np.diag((1., 1., 1.)), [[.5, .5, .5]], ["H"])
    wrapper = kernel.ScalarFunctionWrapper(
        cell,
        [potentials.on_site_potential_family(v0=0)],
        include_coordinates=True,
        include_vectors=True,
    )
    v = 0.1
    dynamics = dyn.PRDynamics(
        wrapper,
        masses=np.ones_like(cell.coordinates),
        masses_v=np.ones_like(cell.vectors),
        v0_v=np.full_like(cell.vectors, v),
    )
    p, snapshots = dyn.integrate(dynamics, 10, snapshots=True)
    for i in snapshots:
        testing.assert_allclose(i.vectors, cell.vectors + v * i.meta["time"])
        testing.assert_allclose(i.coordinates, cell.coordinates)
        testing.assert_allclose(i.meta["vectors_dot"], v)
        testing.assert_allclose(i.meta["coordinates_dot"], 0)


def test_pr_free_dynamics_coords():
    """Free dynamics coordinates only"""
    cell = kernel.Cell(np.diag((1., 1., 1.)), [[.5, .5, .5]], ["H"])
    wrapper = kernel.ScalarFunctionWrapper(
        cell,
        [potentials.on_site_potential_family(v0=0)],
        include_coordinates=True,
        include_vectors=True,
    )
    v = 0.04
    dynamics = dyn.PRDynamics(
        wrapper,
        masses=np.ones_like(cell.coordinates),
        masses_v=np.ones_like(cell.vectors),
        v0=np.array([[v, 0, 0]]),
    )
    p, result = dyn.integrate(dynamics, 10, snapshots=True)
    for i in result:
        testing.assert_allclose(i.meta["ek"].sum() + i.meta["_ek-vectors"].sum(), v ** 2 / 2, rtol=1e-5)
